<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Serve angularjs boot html
 */
$router->get('/', function() {
  return view('index');
});

/**
 * Serve /api
 */
$router->group(['prefix' => 'api'], function() use ($router) {
        $router->post('processCubeSummationInput', 'CubeSummationController@processTestCasesInput');
});
