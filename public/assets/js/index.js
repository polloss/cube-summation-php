$(function() {
    console.log( "ready!" );

    // setup listeners
    $("#submit").on("click", onSubmit);

    function onSubmit(e) {
      e.preventDefault();
      submitButtonEnabled(false);
      var inputContents = $('#input-cases').val();

      console.log(inputContents.split("\n"));

      if (inputContents.split("\n").length < 3) {
        setError(
          true,
          '<ERROR: Los datos de entrada deben contener al menos un caso de prueba>'
        );
        submitButtonEnabled(true);
      } else {
        submitInput(inputContents);
      }
    }

    function submitButtonEnabled(enabled) {
      if (enabled === true) {
        $('#submit').removeClass('disabled');
        $('.process-go-icon').show();
        $('.process-working-icon').hide();
      } else {
        $('#submit').addClass('disabled');
        $('.process-go-icon').hide();
        $('.process-working-icon').show();
      }
    }

    function setError(error, message = '') {
      if (error === true) {
        $('#input-cases-container').addClass('has-danger');
        $('#output-text').text(message);
      } else {
        $('#input-cases-container').removeClass('has-danger');
        $('#output-text').text('');
      }
    }

    function setResult(result) {
        setError(false);
        $('#output-text').text(result);
    }

    function submitInput(input) {
      $.post('./api/processCubeSummationInput', input )
        .done(function(data) {
          console.log(data);
          if (data.output) {
            setResult(data.output);
          } else {
            setError(
              true,
              'ERROR: Ocurrió un error en el servidor'
            );
          }
        })
        .fail(function() {
          setError(
            true,
            'ERROR: Ocurrió un error al procesar los datos de entrada, '
              + 'por favor verifíquelos e intente nuevamente'
          );
        })
        .always(function() {
          submitButtonEnabled(true);
        });
    }

});
