<?php

namespace App\Classes;

use App\Classes\R3Coord;

class R3CoordRangeQuery
{
  public $start;
  public $end;
  public $value;

  public function __construct($startR3Coord, $endR3Coord)
  {
    // assert $startR3Coord instanceof R3Coord
    // assert $endR3Coord instanceof R3Coord

    $this->start = $startR3Coord;
    $this->end = $endR3Coord;
    $this->value = 0;
  }
}
