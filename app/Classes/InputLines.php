<?php

namespace App\Classes;

class InputLines
{
  protected $lines = [];
  protected $index = 0;
  protected $line_count = 0;

  public function setContent($input)
  {
    // Split input in lines
    $this->lines = preg_split('/((\r?\n)|(\r\n?))/', $input);

    // store line count
    $this->count = count($this->lines);

    $this->resetIndex();
  }

  public function resetIndex()
  {
    $this->index = 0;
  }

  public function nextLine()
  {
    if ($this->endReached() === TRUE)
    {
      return '';
    }

    $line = $this->lines[$this->index];
    $this->index += 1;
    return $line;
  }

  public function getLine($index)
  {
    if ($index >= $this->count) {
      return NULL;
    }
    return $this->lines[$index];
  }

  public function endReached()
  {
    return $this->index >= $this->count;
  }
}
