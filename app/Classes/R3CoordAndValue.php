<?php

namespace App\Classes;

use App\Classes\R3Coord;

class R3CoordAndValue
{
  public $coord;
  public $value = 0;

  public function __construct($r3Coord, $value=0)
  {
    // assert $r3Coord instanceof R3Coord
    $this->coord = $r3Coord;
    $this->value = $value;
  }
}
