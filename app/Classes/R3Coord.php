<?php

namespace App\Classes;

class R3Coord
{
  protected $x = 0;
  protected $y = 0;
  protected $z = 0;

  public function __construct($x, $y, $z)
  {
    $this->x = $x;
    $this->y = $y;
    $this->z = $z;
  }

  public function setX($x)
  {
    $this->x = $x;
  }

  public function setY($y)
  {
    $this->y = $y;
  }

  public function setZ($z)
  {
    $this->z = $z;
  }

  public function getX()
  {
    return $this->x;
  }

  public function getY()
  {
    return $this->y;
  }

  public function getZ()
  {
    return $this->z;
  }

  public function isWithinRange($startCoord, $endCoord)
  {
    return
      $this->x >= $startCoord->getX() &&
      $this->y >= $startCoord->getY() &&
      $this->z >= $startCoord->getZ() &&
      $this->x <= $endCoord->getX() &&
      $this->y <= $endCoord->getY() &&
      $this->z <= $endCoord->getZ();
  }

  public function toString()
  {
    return '('.$this->x.', '.$this->y.', '.$this->z.')';
  }

}
