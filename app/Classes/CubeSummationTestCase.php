<?php

namespace App\Classes;

use App\Classes\R3Coord;
use App\Classes\R3CoordRangeQuery;

class CubeSummationTestCase
{
  protected $cubeSize;
  protected $output;

  protected $values;
  protected $queries;

  private static function keyFromCoords($x, $y, $z)
  {
    return $x . ',' . $y . ',' . $z;
  }

  private static function keyFromR3Coords($r3Coords)
  {
    return $r3Coords->getX().','.$r3Coords->getY().','.$r3Coords->getZ();
  }

  public function __construct($cubeSize)
  {
      $this->cubeSize = $cubeSize;
      $this->output = '';
      $this->values = array();
      $this->queries = array();
  }

  public function updateCoord($x, $y, $z, $value)
  {
    // assert 1 <= $x <= $this->cubeSize
    // assert 1 <= $y <= $this->cubeSize
    // assert 1 <= $z <= $this->cubeSize
    // assert -1000000000 <= $value <= 1000000000

    $key = self::keyFromCoords($x, $y, $z);
    $r3Coord = new R3Coord($x, $y, $z);

    if (isset($this->values[$key]))
    {
      $this->values[$key]->value = $value;
    }
    else
    {
      $this->values[$key] = new R3CoordAndValue($r3Coord, $value);
    }
  }

  public function addQueryRange($x1, $y1, $z1, $x2, $y2, $z2)
  {
    $startCoord = new R3Coord($x1, $y1, $z1);
    $endCoord = new R3Coord($x2, $y2, $z2);

    $queryRange = new R3CoordRangeQuery($startCoord, $endCoord);

    $this->queries[] = $queryRange;
  }

  public function processBuffer()
  {
    // return if there is no queries to process
    if (count($this->queries) === 0) return;

    foreach ($this->values as $key => $value) {
      foreach ($this->queries as $queryRange) {
        if ($value->coord->isWithinRange($queryRange->start, $queryRange->end)) {
          $queryRange->value += $value->value;
        }
      }

    }

    foreach ($this->queries as $queryRange) {
      $this->output .= $queryRange->value . "\n";
    }

    // reset queries queue
    $this->queries = array();
  }

  public function getOutput()
  {
    return $this->output;
  }

}
