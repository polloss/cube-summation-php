<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Classes\InputLines;
use App\Classes\CubeSummationTestCase;

class CubeSummationController extends Controller
{
    const X1 = 1;
    const Y1 = 2;
    const Z1 = 3;
    const UPDATE_VALUE = 4;

    const X2 = 4;
    const Y2 = 5;
    const Z2 = 6;

    public function __construct()
    {
        //
    }

    public function processTestCasesInput(Request $request)
    {
      $casesInput = file_get_contents("php://input");

      if (strlen($casesInput) === 0) { //if in test case
        $casesInput = $request->input('inputData');
      }

      $output = '';

      $inputLines = new InputLines();
      $inputLines->setContent($casesInput);

      $testCaseCount = intval($inputLines->nextLine());
      // assert 1 <= $testCaseCount  <= 50

      for($i=0; $i<$testCaseCount; $i++) {
        $caseParamsArr = explode(' ', $inputLines->nextLine());
        // assert count($case_params_arr) === 2

        $cubeSize = intval($caseParamsArr[0]);
        $operationCount = intval($caseParamsArr[1]);
        // assert 1 <= $cubeSize <= 100
        // assert 1 <= $operationCount <= 1000

        $testCase = new CubeSummationTestCase($cubeSize);

        for($j=0; $j<$operationCount; $j++) {
          $line = $inputLines->nextLine();

          $line_array = explode(' ', $line);
          // assert count($line_array) === 5 OR count($line_array) === 7

          $command = $line_array[0];

          if ($command === 'UPDATE')
          {
            $testCase->processBuffer();

            // assert count($line_array) === 5
            $testCase->updateCoord(
              $line_array[self::X1],
              $line_array[self::Y1],
              $line_array[self::Z1],
              $line_array[self::UPDATE_VALUE]
            );
          }
          else if ($command === 'QUERY')
          {
            // assert count($line_array) === 7
            $testCase->addQueryRange(
              $line_array[self::X1],
              $line_array[self::Y1],
              $line_array[self::Z1],
              $line_array[self::X2],
              $line_array[self::Y2],
              $line_array[self::Z2]
            );
          }
        } // end for ($operationCount)

        $testCase->processBuffer();

        $output .= $testCase->getOutput();
      } // end for ($testCaseCount)

      return response()->json(['output' => $output]);
    }

}
