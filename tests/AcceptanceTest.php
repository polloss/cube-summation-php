<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
  public function testBaseCase()
    {
      $input_data = <<<DATA
2
4 5
UPDATE 2 2 2 4
QUERY 1 1 1 3 3 3
UPDATE 1 1 1 23
QUERY 2 2 2 4 4 4
QUERY 1 1 1 3 3 3
2 4
UPDATE 2 2 2 1
QUERY 1 1 1 1 1 1
QUERY 1 1 1 2 2 2
QUERY 2 2 2 2 2 2
DATA;

      $expected_output = <<<DATA
4
4
27
0
1
1

DATA;
        $this->post('/api/processCubeSummationInput', ['inputData' => $input_data])
             ->seeJson([
               'output' => $expected_output
             ]);
    }

    public function testIndexFrontend() {
      $response = $this->call('GET', '/');
      $this->assertEquals(200, $response->status());
    }
}
