<html>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  
  <script src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script defer src="{{ url('assets/js/index.js') }}"></script>
    <body>
      <div class="jumbotron">
        <div class="container">
          <h1 class="display-4">Cube Summation</h1>
          <p>Solución a un Coding Challenge tomado de HackerRank.</p>
          <p>
            <a class="btn btn-primary"
              href="https://www.hackerrank.com/challenges/cube-summation/problem"
              role="button">
                Ver en HackerRank »
            </a>
          </p>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2>Entrada (casos)</h2>
            <div class="form-group" id="input-cases-container">
              <textarea class="form-control" rows="12" id="input-cases"></textarea>
            </div>
            <a class="btn btn-secondary btn-block" id="submit" href="#" role="button">
              Procesar
              <i class="fas fa-angle-double-right fa-sm process-go-icon"></i>
              <i class="fas fa-cog fa-spin process-working-icon" style="display:none;"></i>
            </a>
          </div>
          <div class="col-md-6" id="output-div">
            <h2>Salida</h2>
            <textarea class="form-control" rows="12" id="output-text" disabled
              >Escriba la data de entrada y presione el botón 'Procesar'</textarea>
          </div>
        </div>
      </div>
    </body>
</html>
